#include "kernel/types.h"
#include "user/user.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("error: invalid argc!\n");
    return 1;
  }
  int ticks = atoi(argv[0]);
  sleep(ticks);
  return 0;
}
