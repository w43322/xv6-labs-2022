#include "kernel/types.h"
#include "user/user.h"

int main(int argc, char *argv[]) {
  int p[2];
  pipe(p);
  int r_fd = p[0];
  int w_fd = p[1];
  for (int i = 2; i <= 35; ++i) {
    write(w_fd, &i, sizeof(int));
  }
  close(w_fd);

  if (fork() == 0) {
    while(1) {
      pipe(p);
      w_fd = p[1];
      int k, n;
      int c = 0;
      read(r_fd, &k, sizeof(int));
      printf("prime %d\n", k);
      while(read(r_fd, &n, sizeof(int))) {
        if (n % k) {
          write(w_fd, &n, sizeof(int));
          ++c;
        }
      }
      close(r_fd);
      close(w_fd);
      r_fd = p[0];
      if (c == 0) {
        close(r_fd);
      } else if (fork() == 0) {
        continue;
      }
      break;
    }
  }

  wait((int*)0);

  return 0;
}
