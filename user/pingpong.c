#include "kernel/types.h"
#include "user/user.h"

int main(int argc, char *argv[]) {
  char c;
  int p_p2c[2], p_c2p[2];
  pipe(p_p2c);
  pipe(p_c2p);
  int pid = fork();
  if (pid > 0) { // in parent
    write(p_p2c[1], "", 1);
    read(p_c2p[0], &c, 1);
    printf("%d: received pong\n", getpid());
  } else if (pid == 0) { // in child
    read(p_p2c[0], &c, 1);
    printf("%d: received ping\n", getpid());
    write(p_c2p[1], "", 1);
  } else {
    return 1;
  }
  return 0;
}
